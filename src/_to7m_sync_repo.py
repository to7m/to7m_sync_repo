from pathlib import Path
from shutil import rmtree
from subprocess import run


def strip_caches(dir_):
    for path in dir_.iterdir():
        if path.is_dir():
            if path.name == "__pycache__":
                rmtree(path)
            else:
                strip_caches(path)


def cleanup_repo(repo_dir):
    src = repo_dir / "src"
    for path in src.iterdir():
        if path.name.endswith(".egg-info"):
            rmtree(path)
            continue

    for dir_ in (repo_dir / "src", repo_dir / "tests", repo_dir / "bin"):
        if dir_.exists():
            strip_caches(dir_)


def git_push(repo_dir):
    run(["git", "add", '.'])
    commit_msg = input("commit message: ")
    run(["git", "commit", '-m', commit_msg])
    run(["git", "push", "origin", "master"])


def git_init(repo_dir):
    gitlab_address = f"https://gitlab.com/to7m/{repo_dir.name}.git"
    run(["git", "init"])
    run(["git", "add", '.'])
    run(["git", "commit", '-m', "Initial commit"])
    run(["git", "remote", "add", "origin", gitlab_address])
    run(["git", "push", "--set-upstream", gitlab_address, "master"])
    input(f"now visit {gitlab_address[:-4]}/edit and set public")


def main():
    repo_dir = Path.cwd()
    cleanup_repo(repo_dir)

    if (repo_dir / ".git").exists():
        git_push(repo_dir)
    else:
        git_init(repo_dir)

    # https://github.com/nengo/nengo/issues/1673
    run(["mv", "pyproject.toml", "_pyproject.toml"])
    run(["pip", "install", '-e', '.'])
    run(["mv", "_pyproject.toml", "pyproject.toml"])
