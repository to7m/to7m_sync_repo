import setuptools
from pathlib import Path


repo_dir = Path(__file__).parent
bin_dir = repo_dir / "bin"

scripts = [f"bin/{script.name}" for script in bin_dir.iterdir()]

setuptools.setup(install_requires=["to7m_context>=0.0.0"], scripts=scripts)
